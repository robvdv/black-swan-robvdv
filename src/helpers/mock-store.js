import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';

export default (props = {}) => configureStore([thunk])(props);
