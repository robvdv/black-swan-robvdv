import tweetValidator, { tweet } from '..';

describe('validate tweet schema', () => {
  const validTweet = {
    tweet: 'duis do in ea veniam nulla excepteur voluptate esse fugiat quis mollit occaecat Lorem adipisicing in sit ad aute occaecat',
    date: '2015-01-17',
    claps: 47,
    userId: 1
  }
  let testData;

  beforeEach(() => {
    testData = { ...validTweet };
  });

  test('positive case tweet field', () => {
    expect(tweet.validate('abc').error).toBeUndefined();
  });

  test('negative case tweet field empty', () => {
    expect(tweet.validate('').error).not.toBeUndefined();
  });

  test('too long tweet field', () => {
    testData.tweet = new Array(281).fill('a').join('');
    expect(tweet.validate('').error).not.toBeUndefined();
  });

  test('positive case', () => {
    expect(tweetValidator.validate(testData).error).toBeUndefined();
  });

  test('missing tweet', () => {
    delete testData.tweet;
    expect(tweetValidator.validate(testData).error.toString()).toBe('ValidationError: "tweet" is required');
  });

  test('empty tweet', () => {
    testData.tweet = '';
    expect(tweetValidator.validate(testData).error.toString()).toBe('ValidationError: "tweet" is not allowed to be empty');
  });

  test('too long tweet', () => {
    testData.tweet = new Array(281).fill('a').join('');
    expect(tweetValidator.validate(testData).error.toString()).toBe('ValidationError: "tweet" length must be less than or equal to 280 characters long');
  });

  test('missing date', () => {
    delete testData.date;
    expect(tweetValidator.validate(testData).error.toString())
      .toBe('ValidationError: "date" is required');
  });

  test('malformed date', () => {
    testData.date = '2020-31-12';
    expect(tweetValidator.validate(testData).error.toString()).toBe('ValidationError: "date" must be in ISO 8601 date format');
  });

  test('missing claps', () => {
    delete testData.claps;
    expect(tweetValidator.validate(testData).error.toString()).toBe('ValidationError: "claps" is required');
  });

  test('malformed claps', () => {
    testData.claps = 'abc';
    expect(tweetValidator.validate(testData).error.toString()).toBe('ValidationError: "claps" must be a number');
  });

  test('missing userId', () => {
    delete testData.userId;
    expect(tweetValidator.validate(testData).error.toString()).toBe('ValidationError: "userId" is required');
  });

  test('malformed userId', () => {
    testData.userId = 'abc';
    expect(tweetValidator.validate(testData).error.toString()).toBe('ValidationError: "userId" must be a number');
  });
});
