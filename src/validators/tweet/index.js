import {
  TWEET_MIN_LENGTH,
  TWEET_MAX_LENGTH,
} from 'enum/consts'

import Joi from 'joi';

const tweet = Joi.string()
  .min(TWEET_MIN_LENGTH)
  .max(TWEET_MAX_LENGTH)
  .required();

const schema = Joi.object({
  tweet,

  date: Joi.date()
    .required()
    .iso(),

  claps: Joi.number()
    .required()
    .positive(),

  userId: Joi.number()
    .required()
    .positive(),
});

export { schema as default, tweet };
