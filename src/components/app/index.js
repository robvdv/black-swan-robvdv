import React from 'react'
import { Provider } from 'react-redux';
import TweetPanel from 'components/tweet-panel';
import store from 'store';
import styles from './scss/app.scss'; /* eslint-disable-line no-unused-vars */

export const App = () => {
  return (
    <Provider store={store}>
      <div className="app-wrapper">
        <TweetPanel />
      </div>
    </Provider>
  )
}
