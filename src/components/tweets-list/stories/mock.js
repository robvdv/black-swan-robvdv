export default [
  {
    id: 0,
    tweet: 'duis do in ea veniam nulla excepteur voluptate esse fugiat quis mollit occaecat Lorem adipisicing',
    date: '2015-01-17',
    claps: 47,
    userId: 1
  },
  {
    id: 1,
    tweet: 'duis labore tempor eiusmod culpa in ex nostrud magna id qui fugiat ad commodo deserunt',
    date: '2016-02-20',
    claps: 30,
    userId: 1
  },
  {
    id: 2,
    tweet: 'minim ut consequat occaecat cupidatat minim Lorem occaecat amet aliqua qui labore dolor',
    date: '2015-05-06',
    claps: 11,
    userId: 2
  },
];
