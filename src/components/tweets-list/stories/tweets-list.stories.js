import React from 'react';
import { withKnobs } from '@storybook/addon-knobs';
import mockData from './mock';
import TweetsList from '../index';

export default {
  component: TweetsList,
  title: 'Components/TweetsList',
  decorators: [withKnobs],
};

export const SearchPanelBasic = () => {
  return (
    <div style={{ backgroundColor: 'lightblue', padding: '2rem' }}>
      <TweetsList
        tweets={mockData}
      />
    </div>
  );
};
