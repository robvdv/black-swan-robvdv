import { connect } from 'react-redux';
import { tweetsListFetchAction, } from 'store/tweets/list/action';
import { tweetCreateAction } from 'store/tweet/create/action';
import TweetsList from 'components/tweets-list';

const mapStateToProps = ({
  tweetsList: {
    response: tweets,
  } = {},
}, {
  match: {
    params: {
      mode,
    } = {},
  } = {},
}) => ({
  tweets,
  mode,
});

const mapDispatchToProps = dispatch => ({
  tweetsListFetch: () => dispatch(tweetsListFetchAction()),
  tweetCreate: () => dispatch(tweetCreateAction()),
});

export default connect(mapStateToProps, mapDispatchToProps)(TweetsList);
