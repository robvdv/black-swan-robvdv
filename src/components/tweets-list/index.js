import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import TweetItem from 'components/tweet-item';
import styles from './scss/tweets-list.scss'; /* eslint-disable-line no-unused-vars */

const TweetsList = ({
  tweets,
  tweetsListFetch,
}) => {
  useEffect(() => {
    tweetsListFetch();
  }, [tweetsListFetch]);

  return (
    <div className="tweets-list-wrapper">
      {tweets.map(tweet => (
        <div key={tweet.id} className="tweet-item-wrapper">
          <TweetItem {...tweet}/>
        </div>
      ))}
    </div>
  );
}

TweetsList.defaultProps = {
  tweets: [],
  tweetsListFetch: /* istanbul ignore next */ () => {},
};

TweetsList.propTypes = {
  /** collection of tweets */
  tweets: PropTypes.arrayOf(PropTypes.shape({})),
  /** function to fetch tweets */
  tweetsListFetch: PropTypes.func,
};

export default TweetsList;
