import React, { useEffect, useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import Button from 'components/button';
import Avatar from 'components/avatar';
import TextArea from 'components/textarea';
import { tweet as tweetBodyValidator } from 'validators/tweet';
import styles from './scss/tweet-create.scss'; /* eslint-disable-line no-unused-vars */

const TweetCreate = ({
  initialTweetBody,
  tweetCreate,
  tweetsListFetch,
}) => {
  const [isValid, setIsValid] = useState(false);
  const [tweetBody, setTweetBody] = useState(initialTweetBody);

  useEffect(() => {
    setIsValid(!!tweetBodyValidator.validate(tweetBody).error);
  }, [tweetBody])

  const createAndReload = useCallback(() => {
    // TODO: error handling
    tweetCreate(tweetBody)
      .then(() => {
        setTweetBody('');
        tweetsListFetch();
      })
  }, [tweetBody, tweetCreate, tweetsListFetch])

  return (
    <div className="tweet-create">
      <div className="container">
        <Avatar />
        <div className="form-wrapper">
          <div className="form">
            <TextArea
              onChange={value => setTweetBody(value)}
              html={tweetBody}
            />
          </div>
        </div>
      </div>
      <div className="button-wrapper">
        <Button
          disabled={isValid}
          onClick={createAndReload}
        >Tweet</Button>
      </div>
    </div>
  );
}

TweetCreate.defaultProps = {
  tweetCreate: /* istanbul ignore next */ () => {},
};

TweetCreate.propTypes = {
  /** initial value for tweet body */
  initialTweetBody: PropTypes.string,
  /** function to create a tweet */
  tweetCreate: PropTypes.func,
  /** function to retrieve all tweets */
  tweetsListFetch: PropTypes.func,
};

export default TweetCreate;
