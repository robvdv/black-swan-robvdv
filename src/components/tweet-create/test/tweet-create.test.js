import React from 'react';
import {
  render,
  screen,
  fireEvent,
  act,
} from '@testing-library/react';
import TweetCreate from '..';

describe('tweetCreate component', () => {
  let props;

  const view = () => render(<TweetCreate {...props}>Label</TweetCreate>);

  beforeEach(() => {
    props = {};
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('snapshot', () => {
    view();
    expect(screen).toMatchSnapshot();
  });

  test('visible components', async () => {
    view();
    expect(screen.getAllByRole('button')).toHaveLength(1);
    expect(screen.getAllByText('What\'s happening?')).toHaveLength(1);
  });

  test('does not call tweetCreate when no tweet body', async () => {
    props.tweetCreate = jest.fn();
    view();
    fireEvent.click(screen.getByRole('button'));
    expect(props.tweetCreate).not.toHaveBeenCalled();
  });

  test('calls tweetCreate when valid body', async () => {
    props.tweetsListFetch = jest.fn();
    props.tweetCreate = jest.fn();
    props.tweetCreate.mockImplementation(() => Promise.resolve())
    props.initialTweetBody = 'abc';
    view();
    await act(async () => {
      fireEvent.click(screen.getByRole('button'));
      await expect(props.tweetCreate).toHaveBeenCalled();
      expect(props.tweetsListFetch).toHaveBeenCalled();
    });
  });
});
