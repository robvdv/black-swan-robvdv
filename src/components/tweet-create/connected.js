import { connect } from 'react-redux';
import { tweetCreateAction } from 'store/tweet/create/action';
import { tweetsListFetchAction } from 'store/tweets/list/action';
import TweetCreate from 'components/tweet-create';

const mapStateToProps = ({
  tweetCreate: {
    response: tweet,
  } = {},
}) => ({
  tweet,
});

const mapDispatchToProps = dispatch => ({
  tweetCreate: (tweet) => dispatch(tweetCreateAction(tweet)),
  tweetsListFetch: (tweet) => dispatch(tweetsListFetchAction(tweet)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TweetCreate);
