import React from 'react';
import { withKnobs } from '@storybook/addon-knobs';
import TweetCreate from '../index';

export default {
  component: TweetCreate,
  title: 'Components/TweetCreate',
  decorators: [withKnobs],
};

export const TweetCreateBasic = () => {
  return (
    <div style={{ backgroundColor: 'lightblue', padding: '2rem' }}>
      <TweetCreate
        isSubmitEnabled={false}
      />
    </div>
  );
};
