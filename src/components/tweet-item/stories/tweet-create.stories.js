import React from 'react';
import { withKnobs, text } from '@storybook/addon-knobs';
import TweetItem from '../index';

export default {
  component: TweetItem,
  title: 'Components/TweetItem',
  decorators: [withKnobs],
};

export const TweetItemBasic = () => {
  return (
    <div style={{ backgroundColor: 'lightblue', padding: '2rem' }}>
      <TweetItem
        tweet={text('tweet', 'Lorum ipsum')}
        userId={text('userId', '123')}
        claps={text('claps', '67')}
        date={text('date', '2021-12-31')}
      />
    </div>
  );
};
