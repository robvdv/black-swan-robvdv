import React from 'react';
import PropTypes from 'prop-types';
import Avatar from 'components/avatar';
import Claps from 'components/claps';
import styles from './scss/tweet-item.scss'; /* eslint-disable-line no-unused-vars */

const TweetItem = ({
  tweet,
  date,
  claps,
  userId
}) => {
  // TODO: consider sanitize_html or markdown
  return (
    <div className="tweet-item">
      <div className="container">
        <Avatar userId={userId} />
        <div className="tweet-wrapper">
          <div className="text-wrapper" dangerouslySetInnerHTML={{ __html: tweet }} />
          <div className="footer-wrapper">
            <Claps claps={claps} />
          </div>
        </div>
      </div>

    </div>
  );
}

TweetItem.defaultProps = {
  tweetItem: /* istanbul ignore next */ () => {},
};

TweetItem.propTypes = {
  /** function to item a tweet tweets */
  tweetItem: PropTypes.func,
  /** tweet text */
  tweet: PropTypes.string,
  /** string representation of the date */
  date: PropTypes.string,
  /** the number of claps */
  claps: PropTypes.number,
  /** id number of the user */
  userId: PropTypes.number,
};

export default TweetItem;
