import React from 'react';
import PropTypes from 'prop-types';
import styles from './scss/button.scss'; /* eslint-disable-line no-unused-vars */

const Button = ({
  disabled,
  busy,
  children,
  onClick,
}) => {
  return (
    <button
      className={`btn ${busy ? 'busy' : ''}`}
      disabled={disabled}
      onClick={onClick}
    >
      {children}
    </button>
  );
}

Button.defaultProps = {
  disabled: false,
  busy: false,
  onClick: /* istanbul ignore next */ () => {},
};

Button.propTypes = {
  /** if true the button will be marked as disabled */
  disabled: PropTypes.bool,
  /** if true the button be styled as busy */
  busy: PropTypes.bool,
  /** child node of this component */
  children: PropTypes.node,
  /** function to call when button is clicked */
  onClick: PropTypes.func,
};

export default Button;
