import React from 'react';
import { withKnobs, boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import Button from '../index';

export default {
  component: Button,
  title: 'Components/Button',
  decorators: [withKnobs],
};

export const ButtonBasic = () => {
  return (
    <div style={{ backgroundColor: 'lightblue', padding: '2rem' }}>
      <Button
        disabled={boolean('disabled', false)}
        busy={boolean('busy', false)}
        onClick={action('onClick triggered')}
      >
        Click
      </Button>
    </div>
  );
};
