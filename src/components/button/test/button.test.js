import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Button from '..';

describe('button component', () => {
  let props;

  const view = () => render(<Button {...props}>Label</Button>);

  beforeEach(() => {
    props = {};
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('snapshot', () => {
    view();
    expect(screen).toMatchSnapshot();
  });

  test('onClick', async () => {
    props.onClick = jest.fn();
    view();
    fireEvent.click(screen.getByRole('button'));
    expect(props.onClick).toHaveBeenCalled();
  });

  test('disabled', async () => {
    props.disabled = true;
    view();
    expect(screen.getByRole('button').hasAttribute('disabled')).toBeTruthy();
  });

  test('not busy', async () => {
    props.busy = false;
    view();
    expect(document.getElementsByClassName('busy').length).toBe(0);
  });

  test('busy', async () => {
    props.busy = true;
    view();
    expect(document.getElementsByClassName('busy').length).toBe(1);
  });
});
