import React from 'react';
import styles from './scss/avatar.scss'; /* eslint-disable-line no-unused-vars */

const Avatar = () => (
  <div className="avatar">
    <div className="icon" />
  </div>
);

export default Avatar;
