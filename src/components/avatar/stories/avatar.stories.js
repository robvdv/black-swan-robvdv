import React from 'react';
import { withKnobs, text } from '@storybook/addon-knobs';

import Avatar from '../index';

export default {
  component: Avatar,
  title: 'Components/Avatar',
  decorators: [withKnobs],
};

export const AvatarBasic = () => {
  return (
    <div style={{ backgroundColor: 'lightblue', padding: '2rem' }}>
      <Avatar
        userId={text('userId', '123')}
      />
    </div>
  );
};
