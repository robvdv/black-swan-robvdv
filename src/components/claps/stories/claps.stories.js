import React from 'react';
import { withKnobs, text } from '@storybook/addon-knobs';

import Claps from '../index';

export default {
  component: Claps,
  title: 'Components/Claps',
  decorators: [withKnobs],
};

export const ClapsBasic = () => {
  return (
    <div style={{ backgroundColor: 'lightblue', padding: '2rem' }}>
      <Claps
        claps={text('claps', '123')}
      />
    </div>
  );
};
