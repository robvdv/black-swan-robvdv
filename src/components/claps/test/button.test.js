import React from 'react';
import { render, screen } from '@testing-library/react';
import Claps from '..';

describe('claps component', () => {
  let props;

  const view = () => render(<Claps {...props}>Label</Claps>);

  beforeEach(() => {
    props = {};
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('snapshot', () => {
    view();
    expect(screen).toMatchSnapshot();
  });

  test('claps zero', async () => {
    view();
    props = {
      claps: 0,
    };
    expect(screen.queryByText('0')).toBe(null);
  });

  test('claps non-zero', async () => {
    view();
    props = {
      claps: 123,
    };
    expect(screen.queryByText('123')).toBe(null);
  });
});
