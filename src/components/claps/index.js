import React from 'react';
import PropTypes from 'prop-types';
import styles from './scss/claps.scss'; /* eslint-disable-line no-unused-vars */

const Claps = ({
  claps,
}) => {
  return (
    <div className="claps-wrapper">
      <div className="icon"/>
      <div className="clap-count">{claps > 0 ? claps : ''}</div>
    </div>
  );
}

Claps.defaultProps = {
  claps: 0,
};

Claps.propTypes = {
  /** seed value for the background text */
  claps: PropTypes.number,
};

export default Claps;
