import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TweetCreate from 'components/tweet-create/connected';
import TweetsList from 'components/tweets-list/connected';
import styles from './scss/tweet-panel.scss'; /* eslint-disable-line no-unused-vars */

class TweetPanelWrapper extends Component {
  render() {
    const {
      tweets,
    } = this.props;

    return (
      <div
        className="tweet-panel-wrapper"
      >
        <div className="tweet-create-wrapper">
          <TweetCreate />
        </div>
        <div className="tweets-list-wrapper">
          <TweetsList tweets={tweets} />
        </div>
      </div>
    );
  }
}

TweetPanelWrapper.defaultProps = {
  tweets: [],
};

TweetPanelWrapper.propTypes = {
  /** tweets to display */
  tweets: PropTypes.arrayOf(PropTypes.shape({})),
};

export default TweetPanelWrapper;
