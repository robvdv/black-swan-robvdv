import React from 'react';
import { withKnobs, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import TextArea from '../index';

export default {
  component: TextArea,
  title: 'Components/Textarea',
  decorators: [withKnobs],
};

export const TextareaBasic = () => {
  return (
    <div style={{ backgroundColor: 'lightblue', padding: '2rem' }}>
      <TextArea
        onChange={action('onChange triggered')}
        html={text('html', '')}
      />
    </div>
  );
};
