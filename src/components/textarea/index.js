import React, { useCallback, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import ContentEditable from 'react-contenteditable';
import styles from './scss/textarea.scss'; /* eslint-disable-line no-unused-vars */

const TextArea = ({
  onChange,
  html,
}) => {
  const [hasFocus, setHasFocus] = useState(false);
  const [value, setValue] = useState(html);

  const handleChange = useCallback((event) => {
    const newValue = event.currentTarget.innerHTML;
    onChange(newValue);
    setValue(newValue);
  }, [onChange]);

  const ref = useRef(null);

  return (
    <div className="textarea-wrapper">
      {!hasFocus && !value && <span className="placeholder">What&apos;s happening?</span>}
      <ContentEditable
        className="textarea"
        ref={ref}
        html={html}
        onChange={handleChange}
        onFocus={() => setHasFocus(true)}
        onBlur={() => setHasFocus(false)}
      />
    </div>
  );
}

TextArea.defaultProps = {
  disabled: false,
  html: '',
  onChange: /* istanbul ignore next */ () => {},
};

TextArea.propTypes = {
  /** called when contents of textarea changes */
  onChange: PropTypes.func,
  /** html body of the component */
  html: PropTypes.string,
};

export default TextArea;
