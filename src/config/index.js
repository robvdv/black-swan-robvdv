const env = process.env;

const config = {
  ...env
}

export default config;
