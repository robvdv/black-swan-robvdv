import { USER_ID_DEFAULT } from 'enum/consts';

class Tweet {
  constructor({
    tweet,
    date = new Date(),
    claps = 0,
    userId = USER_ID_DEFAULT,
  }) {
    this.tweet = tweet;
    this.date = date;
    this.claps = claps;
    this.userId = userId;
  }

  serialize() {
    return {
      tweet: this.tweet,
      date: this.date.toISOString(),
      claps: this.claps,
      userId: this.userId,
    }
  }
}

export default Tweet;
