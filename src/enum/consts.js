export const TWEET_MIN_LENGTH = 1;
export const TWEET_MAX_LENGTH = 280;

export const USER_ID_DEFAULT = 5000;
