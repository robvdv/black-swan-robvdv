import {
  post,
} from 'api/fetcher';

import { tweetCreate } from '..';
jest.mock('api/fetcher', () => ({
  post: jest.fn(),
}));

describe('tweet create', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  test('tweetCreate plain function', async () => {
    await tweetCreate({ foo: 'bar' });
    expect(post).toBeCalledWith('/tweets', { body: '{"foo":"bar"}' });
  });
});
