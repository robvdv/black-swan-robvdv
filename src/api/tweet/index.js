import { post } from '../fetcher';

const tweetCreate = tweet => post('/tweets', { body: JSON.stringify(tweet) });

export {
  tweetCreate,
};
