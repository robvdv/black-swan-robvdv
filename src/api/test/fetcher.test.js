import {
  fetcher,
  get,
  post,
} from '../fetcher';

jest.mock('config', () => ({
  REACT_APP_API_URL: '/api',
}));

describe('fetcher', () => {
  const resolveFn = () =>
    Promise.resolve({
      json: () => Promise.resolve({ resolve: true }),
    });
  const rejectFn = () =>
    Promise.resolve({
      json: () => Promise.resolve({ reject: true }),
    });
  const errorFn = () => {
    throw new Error('failure');
  }

  beforeEach(() => {
    global.fetch = jest.fn(resolveFn);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('fetcher plain function success', async () => {
    const response = await fetcher('/abc', { foo: 'bar' });
    expect(global.fetch).toBeCalledWith('/api/abc', { foo: 'bar', headers: { 'Content-Type': 'application/json' } });
    expect(response).toEqual({ resolve: true });
  });

  test('fetcher plain function failure', async () => {
    global.fetch = jest.fn(rejectFn);
    const response = await fetcher('/abc', { foo: 'bar' });
    expect(global.fetch).toBeCalledWith('/api/abc', { foo: 'bar', headers: { 'Content-Type': 'application/json' } });
    expect(response).toEqual({ reject: true });
  });

  test('fetcher plain function error', async () => {
    global.fetch = jest.fn(errorFn);
    try {
      await fetcher('/abc', { foo: 'bar' });
      expect('Unreachable').toBe(false);
    } catch (error) {
      expect(error.message).toBe('Error calling API: Error: failure');
    }
  });

  test('get function', async () => {
    global.fetch = jest.fn(rejectFn);
    const response = await get('/abc');
    expect(global.fetch).toBeCalledWith('/api/abc', {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    });
    expect(response).toEqual({ reject: true });
  });

  test('post function', async () => {
    global.fetch = jest.fn(rejectFn);
    const response = await post('/abc', { body: "{ foo: 'bar' }" });
    expect(global.fetch).toBeCalledWith('/api/abc', {
      method: 'POST',
      body: "{ foo: 'bar' }",
      headers: { 'Content-Type': 'application/json' },
    });
    expect(response).toEqual({ reject: true });
  });
});
