import config from 'config';

const fetcher = (pathname, { ...rest }) => {
  try {
    const url = `${config.REACT_APP_API_URL}${pathname}`;
    return fetch(url, {
      ...rest,
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => response.json());
  } catch (e) {
    return Promise.reject(new Error(`Error calling API: ${e}`));
  }
}

const get = (pathname, { ...rest }) => fetcher(pathname, { ...rest, method: 'GET' });
const post = (pathname, { ...rest }) => fetcher(pathname, { ...rest, method: 'POST' });

export {
  fetcher,
  get,
  post,
};
