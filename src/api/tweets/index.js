import { get } from '../fetcher';

const tweetsListFetch = () => get('/tweets');

export {
  tweetsListFetch,
};
