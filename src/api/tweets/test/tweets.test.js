import {
  get,
} from 'api/fetcher';

import { tweetsListFetch } from '..';

jest.mock('api/fetcher', () => ({
  get: jest.fn(),
}));

describe('tweets list', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  test('tweetCreate plain function', async () => {
    await tweetsListFetch();

    expect(get).toBeCalledWith('/tweets');
  });
});
