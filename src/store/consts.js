const STATUS = {
  BUSY: 'BUSY',
  SUCCESS: 'SUCCESS',
  FAILURE: 'FAILURE',
};

export {
  STATUS,
}
