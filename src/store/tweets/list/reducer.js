import {
  BUSY,
  SUCCESS,
  FAILURE,
} from './consts';
import { STATUS } from 'store/consts';

const reducer = (state = {}, { type, payload }) => {
  switch (type) {
    case BUSY: {
      return {
        status: STATUS.BUSY
      };
    }
    case SUCCESS: {
      return {
        response: payload,
        status: STATUS.SUCCESS,
      };
    }
    case FAILURE: {
      return {
        error: payload,
        status: STATUS.FAILURE
      };
    }
    default:
      return state;
  }
};

export default reducer;
