import reducer from '../reducer';

describe('tweet reducer', () => {
  const startState = { start: 1 };

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('empty state', async () => {
    expect(reducer(undefined, {})).toEqual({});
  });

  test('default', async () => {
    const action = {
      type: 'UNKNOWN',
    };
    expect(reducer(startState, action)).toEqual(startState);
  });

  test('sets busy', async () => {
    const action = {
      type: 'TWEETS_LIST_BUSY',
    };
    expect(reducer(startState, action)).toEqual({
      status: 'BUSY'
    });
  });

  test('sets response on success', async () => {
    const action = {
      type: 'TWEETS_LIST_SUCCESS',
      payload: { tweet: 'tweet' },
    };
    expect(reducer(startState, action)).toEqual({
      response: {
        tweet: 'tweet'
      },
      status: 'SUCCESS'
    });
  });

  test('sets failure', async () => {
    const action = {
      type: 'TWEETS_LIST_FAILURE',
      payload: 'err',
    };
    expect(reducer(startState, action)).toEqual({
      error: 'err',
      status: 'FAILURE'
    });
  });
});
