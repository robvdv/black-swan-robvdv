import { tweetsListFetchAction } from '../action';
import { tweetsListFetch } from 'api/tweets';
import configureStore from 'helpers/mock-store'

jest.mock('api/tweets');

describe('tweets action', () => {
  let store;

  beforeEach(() => {
    store = configureStore();
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('positive case fetch tweets action', async () => {
    tweetsListFetch.mockImplementation(() => {
      return Promise.resolve([{ tweet: 1 }])
    });
    await store.dispatch(tweetsListFetchAction());

    expect(store.getActions()).toEqual([
      {
        type: 'TWEETS_LIST_BUSY'
      },
      {
        payload: [{ tweet: 1 }],
        type: 'TWEETS_LIST_SUCCESS'
      }
    ]);
  });

  test('negative case fetch tweet action', async () => {
    tweetsListFetch.mockImplementation((tweet) => {
      return Promise.reject(new Error('bad api'))
    });
    await store.dispatch(tweetsListFetchAction());

    expect(store.getActions()).toEqual([
      {
        type: 'TWEETS_LIST_BUSY'
      },
      {
        payload: expect.any(Object),
        type: 'TWEETS_LIST_FAILURE'
      }
    ]);
  });
});
