import {
  BUSY,
  SUCCESS,
  FAILURE,
} from './consts';
import { tweetsListFetch } from 'api/tweets'

const tweetsListFetchAction = () => async dispatch => {
  dispatch({
    type: BUSY,
  });

  try {
    const response = await tweetsListFetch();

    dispatch({
      type: SUCCESS,
      payload: response,
    });
  } catch (error) {
    dispatch({
      type: FAILURE,
      payload: error,
    });
  }
};

export { tweetsListFetchAction };
