import {
  createStore,
  combineReducers,
  applyMiddleware,
  compose,
} from 'redux';

import thunkMiddleware from 'redux-thunk'

import tweetsListReducer from 'store/tweets/list/reducer';
import tweetCreateReducer from 'store/tweet/create/reducer';

const rootReducer = combineReducers({
  tweetsList: tweetsListReducer,
  tweetCreate: tweetCreateReducer,
});

const devConfig = [];

if (window.__REDUX_DEVTOOLS_EXTENSION__) devConfig.push(window.__REDUX_DEVTOOLS_EXTENSION__());

const index = createStore(
  rootReducer,
  compose(
    applyMiddleware(
      thunkMiddleware
    ),
    ...devConfig,
  ),
);

export default index;
