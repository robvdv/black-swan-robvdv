import { STATUS } from 'store/consts';

const ENTITY = 'TWEET_LIST';

const BUSY = `${ENTITY}_${STATUS.BUSY}`;
const SUCCESS = `${ENTITY}_${STATUS.SUCCESS}`;
const FAILURE = `${ENTITY}_${STATUS.FAILURE}`;

export {
  ENTITY,
  BUSY,
  SUCCESS,
  FAILURE,
}
