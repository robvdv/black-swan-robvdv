import {
  BUSY,
  SUCCESS,
  FAILURE,
} from './consts';
import Tweet from 'model/tweet';
import { tweetCreate } from 'api/tweet'

const tweetCreateAction = (tweet) => async dispatch => {
  dispatch({
    type: BUSY,
  });

  const tweetModel = new Tweet({ tweet });

  try {
    const response = await tweetCreate(tweetModel.serialize());

    dispatch({
      type: SUCCESS,
      payload: response,
    });
  } catch (error) {
    dispatch({
      type: FAILURE,
      payload: error,
    });
  }
};

export { tweetCreateAction };
