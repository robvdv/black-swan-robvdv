import { tweetCreateAction } from '../action';
import { tweetCreate } from 'api/tweet';
import configureStore from 'helpers/mock-store'

jest.mock('api/tweet');

describe('tweet action', () => {
  let store;

  beforeEach(() => {
    store = configureStore();
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('positive case tweet action', async () => {
    tweetCreate.mockImplementation((tweet) => {
      return Promise.resolve(tweet)
    });
    await store.dispatch(tweetCreateAction({ tweet: 'foo' }));

    expect(store.getActions()).toEqual([
      {
        type: 'TWEET_LIST_BUSY'
      },
      {
        payload: {
          claps: 0,
          date: expect.any(String),
          tweet: {
            tweet: 'foo'
          },
          userId: 5000
        },
        type: 'TWEET_LIST_SUCCESS'
      }
    ]);
  });

  test('negative case tweet action', async () => {
    tweetCreate.mockImplementation((tweet) => {
      return Promise.reject(new Error('bad api'))
    });
    await store.dispatch(tweetCreateAction({ tweet: 'foo' }));

    expect(store.getActions()).toEqual([
      {
        type: 'TWEET_LIST_BUSY'
      },
      {
        payload: expect.any(Object),
        type: 'TWEET_LIST_FAILURE'
      }
    ]);
  });
});
