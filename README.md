# Cygnet - a partial Twitter implementation for Black Swan Data

### Prerequisites
* You'll need to have [git](https://git-scm.com/) and [node](https://nodejs.org/en/) installed in your system.

### To run
* Clone the project:

```
git clone https://gitlab.com/robvdv/black-swan-robvdv.git
```

* Then install the dependencies:

```
npm install -g json-server
npm install
```

* Run development server:
This will start the web server and the JSON server concurrently
```
npm run start
```

Open the web browser to `http://localhost:8080/`

### To run unit tests with coverage
```
npm run test
```
Coverage will be located at: ./coverage/lcov-report/index.html

### To run storybook
```
npm run storybook
```

### Eslint
```
npm run lint
```
