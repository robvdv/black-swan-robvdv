const path = require('path');

module.exports = {
  stories: ['../src/**/*.stories.js'],
  addons: ['@storybook/addon-actions', '@storybook/addon-knobs', '@storybook/addon-links'],
  webpackFinal: async config => {
    // do mutation to the config

    // Make whatever fine-grained changes you need
    config.module.rules.push({
      test: /\.scss$/,
      use: ['style-loader', 'css-loader', 'sass-loader'],
    });

    // pity this isn't reading webpack config. Why do I need to set this here?
    config.resolve.alias.Styles = path.resolve(__dirname, '../src/styles');

    return config;
  },
};
